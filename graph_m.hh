#include "list.hh"
#include "matrix.hh"

#define SIZE 500


template <typename Type, typename eType> //wierzcholek
struct vertex
{
    Type val; // nazwa wierzcholka
    element<vertex<Type,eType>*>* v_list_elem; // wskaznik na liste zawierajaca wszystkie wierzcholki
    int key;
};

template <typename Type, typename eType> //wierzcholek
struct edge
{
    eType val; // dlugosc krawedzi
    vertex<Type,eType>* e1; // wskaznik na pierwszy wierzcholek
    vertex<Type,eType>* e2; // wskaznik na drugi wierzcholek
    element<edge<Type,eType>*>* e_list_elem; // wskaznik na liste zawierajaca wszyskie krawedzie
};

template <typename Type, typename eType> //wierzcholek
class graph_m
{
private:
    list<edge<Type,eType>*> e_list; // lista krawedzi
    list<vertex<Type,eType>*> v_list; //lista wierzcholkow
    Matrix<edge<Type,eType>*,SIZE> adj_m;
    int num_of_keys;
public:
    graph_m(){num_of_keys = 0;};
    bool isEmpty(); // sprawdza czy graf jest pusty
    vertex<Type,eType> * find_vertex(Type val); // znajduje wskaźnik na wierzcholek o nazwie 'val'
    edge<Type,eType> * find_edge(eType val); // znajduje wskaźnik na krawedz o dlugosci 'val'
    void verticies(); // wypisuje wszystkie wierzcholki
    void edges(); // wypisuje wszystkie krawedzie
    void incidentEdges(vertex<Type,eType>* v);
    void insertVertex(Type o); // dodaje wierzcholek o nazwie o
    void insertEdge(Type v, Type w, eType o); // dodaje krawedz e pomiedzy wierzcholkami v i w
    vertex<Type,eType>* end_verticies(edge<Type,eType> * e); // zwraca tablice wierzcholkow na koncach krawedzi e
    vertex<Type,eType>* opposite(vertex<Type,eType>* v, edge<Type,eType>* e); // zwraca wierzcholek przeciwny do v wzgledem krawedzi e
    bool are_Adjacent(vertex<Type,eType>* v, vertex<Type,eType>* w); // sprawdza czy v i w maja wspolna krawedz
    void replace_v(vertex<Type,eType>* v, Type val); // zamienia nazwe wierzcholka v na wartosc val
    void replace_e(edge<Type,eType>* e, eType val); // zamienia dlugosc krawedzi e na wartosc val
    void removeVertex(Type o);
    void removeEdge(eType o);
    void print_matrix()
    {
        for (int i = 0; i < num_of_keys; i++)
        {
            for (int j = 0; j < num_of_keys; j++)
            {
                if (adj_m(i,j) == NULL)
                {
                    std::cout << "0 ";
                }else
                {
                    std::cout << adj_m(i,j)->val << " "; 
                }
                
            }
            std::cout << std::endl;
        }
        
    }

    void show_key(Type o)
    {
        std::cout << find_vertex(0)->key << std::endl;
    }
    void show_v_head()
    {
       std::cout << v_list.get_head()->val->key;
    }
    edge<Type,eType> * give_mat(int a, int b);
};

template <typename Type, typename eType> 
bool graph_m<Type,eType>::isEmpty()
{
    if (v_list.isEmpty()) // sprawdza liste wierzcholkow
    {
        return 1;
    }else return 0;
}

template <typename Type, typename eType> 
vertex<Type,eType>* graph_m<Type,eType>::find_vertex(Type val)
{
    vertex<Type,eType> n;
    n.val = val;
    vertex<Type,eType> *m;
    m = &n; 
    return v_list.find_ptr_elem(m)->val; // przeszkuje liste wierzcholkow w poszukiwaniu tego o nazwie val
}

template <typename Type, typename eType> 
edge<Type,eType>* graph_m<Type,eType>::find_edge(eType val)
{
    edge<Type,eType> n;
    n.val = val;
    edge<Type,eType>* m = &n;
    return e_list.find_ptr_elem(m)->val; // przeszukuje liste krawedzi w poszukiwaniu tego o dlugosci val
}

template <typename Type, typename eType> 
void graph_m<Type,eType>::verticies()
{
    if(!v_list.isEmpty())
    {
        element<vertex<Type,eType>*>* n = v_list.get_head();
        
        while (n != NULL) // jesli lista wierzcholkow nie jest pusta wypisuje nazwy wszystkich wierzcholkow ktore sa jej elementami
        {
            std::cout << (n->val)->val << " ";
            n = n->next;
        }
        std::cout << "\n";   
    }else 
    {
        std::cout << "Brak wierzcholkow \n";
    }
}

template <typename Type, typename eType> 
void graph_m<Type,eType>::edges()
{
    if(!e_list.isEmpty())
    {
        
        element<edge<Type,eType>*>* n = e_list.get_head();

        while (n != NULL) // jesli lista krawedzi nie jest pusta wypisuje dlugosci wszystkich krawedzi ktore sa jej elementami
        {
            std::cout << (n->val->val) << " ";
            n = n->next;
        }
        std::cout << "\n";
    }else
    {
        std::cout << "Brak krawedzi \n";
    }
}

template <typename Type, typename eType> 
void graph_m<Type,eType>::incidentEdges(vertex<Type,eType>* v)
{
    for (int i = 0; i < num_of_keys; i++)
    {
        if (adj_m(v->key,i) != NULL)
        {
            std::cout << adj_m(v->key,i)->val << " ";
        }
    }
    std::cout << std::endl;
}

template <typename Type, typename eType> 
void graph_m<Type,eType>::insertVertex(Type o)
{
    vertex<Type,eType>* v = new vertex<Type,eType>;
    v->val = o;
    v->key = num_of_keys;
    v_list.add_front(v); // dodaje nowy wierzcholek do list wierzcholkow
    v->v_list_elem = v_list.find_elem(v);
    ++num_of_keys;
}

template <typename Type, typename eType> 
void graph_m<Type,eType>::insertEdge(Type v, Type w, eType o)
{
    edge<Type,eType> * e = new edge<Type,eType>;
    vertex<Type,eType> * ver = new vertex<Type,eType>;
    ver->val = v;
    e->val = o;

    e_list.add_front(e); // dodaje nowa krawedz do listy krawedzi

    e->e_list_elem = e_list.find_elem(e); // ustawianie wskaznikow krawedzi na wierzcholki, z oraz na liste incydencji
    e->e1 = v_list.find_ptr_elem(ver)->val;

    ver->val = w;
    e->e2 = v_list.find_ptr_elem(ver)->val;

    adj_m(e->e1->key,e->e2->key) = e;
    adj_m(e->e2->key,e->e1->key) = e;

    delete ver;
}

template <typename Type, typename eType> 
vertex<Type,eType>* graph_m<Type,eType>::end_verticies(edge<Type,eType> *e)
{
    vertex<Type,eType>* tab = new vertex<Type,eType>[2]; //tablica wierzcholkow przy krawedzi e
    tab[1] = *e->e1;
    tab[0] = *e->e2;

    return tab;    
}

template <typename Type, typename eType> 
vertex<Type,eType>* graph_m<Type,eType>::opposite(vertex<Type,eType>* v, edge<Type,eType>* e)
{
    if (e->e1->val == v->val)
    {
        return e->e2;
    }else return e->e1; //wierzcholek przeciwny do v wzgledem krawedzi e
}

template <typename Type, typename eType>
bool graph_m<Type,eType>::are_Adjacent(vertex<Type,eType>* v, vertex<Type,eType>* w)
{
    bool flag = 0;

    if (adj_m(w->key,v->key) != NULL)
    {
        flag = 1;
    }
    
    

    return flag;
}

template <typename Type, typename eType>
void graph_m<Type,eType>::replace_v(vertex<Type,eType>* v, Type val)
{
    v->val = val;
}

template <typename Type, typename eType>
void graph_m<Type,eType>::replace_e(edge<Type,eType>* e, eType val)
{
    e->val = val;
}

template <typename Type, typename eType>
edge<Type,eType> * graph_m<Type,eType>::give_mat(int a, int b)
{
    return adj_m(a,b);
}

template <typename Type, typename eType>
void graph_m<Type,eType>::removeVertex(Type o)
{
    vertex<Type,eType> * n = find_vertex(o);

    for (int i = 0; i < num_of_keys; i++)
    {
        if (adj_m(n->key,i) != NULL)
        {
            removeEdge(adj_m(n->key,i)->val);
        }   
    }

    for (int i = n->key; i < num_of_keys; i++)
    {
        for (int j = n->key; j < num_of_keys; j++)
        {
            adj_m(i,j) = adj_m(i,j++);
        }
    }
    
    v_list.remove(n->v_list_elem->val);
    --num_of_keys;
    
    delete n;

}

template <typename Type, typename eType>
void graph_m<Type,eType>::removeEdge(eType o)
{
    edge<Type,eType>* n = find_edge(o);

    e_list.remove(n->e_list_elem->val);
    adj_m(n->e1->key,n->e2->key) = NULL;
    adj_m(n->e2->key,n->e1->key) = NULL;

    delete n;
}
