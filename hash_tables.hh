#include <iostream>
#include <string>
#include "list.hh"

using namespace std;
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//funkcja sprawdzajaca czy n to liczba pierwsza
bool check_if_prime(int n)
{
    int m=0; 
    m=n/2;  
    for(int i = 2; i <= m; i++)  
    {  
        if(n % i == 0)  
        {  
            return false;
        }  
    }  
    return true;  
}

// tablica haszujaca z linkowaniem
class hash_table
{
private:
    int SIZE; //rozmiar tablicy
    list<int> * table; //wskaznik na liste elementow
public:
    hash_table(int s)
    {
    table = new list<int>[s];
    SIZE = s;
    }
    bool isEmpty(); //sprawdza czy wszystkie listy sa puste
    int hash(int number);
    void insert(int number); //dodaje number do listy o indeksie hash(number)
    void remove(int number); //usuwa number z listy o indeksie hash(number), jesli go nie znajdzie informuje o tym 
    element<int> * find(int number); //zwraca wskaznik na inta z listy
    void entries(); //wyswietla po kolei wszystkie listy
};

bool hash_table::isEmpty()
{
    bool flag = 0;
    for (int i = 0; i < SIZE; i++)
    {
        if (!table[i].isEmpty())
        {
            return false;
        } 
    }  
    return true;
}


int hash_table::hash(int number)
{
    return number%SIZE;
}


void hash_table::insert(int number)
{
    int key = hash(number);
    table[key].add_front(number);
}


void hash_table::remove(int number)
{
    int key = hash(number);
    table[key].remove(number);
}


element<int> * hash_table::find(int number)
{
    int key = hash(number);
    if(table[key].find_elem(number) != NULL)
    {
        return table[key].find_elem(number);
    }
    else return NULL;
}


void hash_table::entries()
{
    if (!isEmpty())
    {
        for (int i = 0; i < SIZE; i++)
        {
            table[i].print_list();
        }
    }else
    {
        cout << "Tablica jest pusta" << endl;
    }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// tablica haszujaca z probkowaniem liniowym
class hash_table_linear
{
private:
    int SIZE; 
    int **table; //wskaznik na tablice wskaznikow na inty
    int * dummy; //pomocniczy wskaznik sluzacy jako wypelniacz po usunieciu elementu
public:
    hash_table_linear(int s)
    {
        table = new int*[s];
        SIZE = s;
        for (int i = 0; i < s; i++)
        {
            table[i] = NULL;
        }
    }

    bool isEmpty();
    int hash(int number);
    void insert(int number);
    int remove(int key);
    int * find(int number);
    void entries();
};

bool hash_table_linear::isEmpty()
{
    bool flag = 0;
    for (int i = 0; i < SIZE; i++)
    {
        if (table[i] != NULL)
        {
            return false;
        } 
    }  
    return true;
}

int hash_table_linear::hash(int number)
{
    return number%SIZE;
}

void hash_table_linear::insert(int number)
{
    int key = hash(number);
    int * tmp = new int; //tworzymy nowego inta o wartosci number
    *tmp = number;
    bool flag = 0;
    int p = 1;
    while (!flag)
    {
        if (table[hash(key)] != NULL && table[hash(key)] != dummy) //jesli miejsce jest zapelnione lub nie ma w nim wypelniacza szukamy nastepnego wolnego miejsca
        {
            key++;
            p++;
        }else
        {
            table[hash(key)] = tmp; //jesli mamy wolne miejsce lub wypelniacz wpisujemy w to miejsce nowy int
            flag = 1;
        }
        if (p >= SIZE) flag = 1; //jesli przeszukamy cala tablice i nie znajdziemy miejsca informujemy o tym
    }
    if (p >= SIZE)
    {
        cout << "Nie udalo sie dodac elementu, brak miejsca w tablicy!" << endl;
    }
    
    cout << "Liczba probek przy dodawaniu: " << p << endl;
}

int hash_table_linear::remove(int key)
{
    if (key >= SIZE)
    {
        key = hash(key);
    }
    
    int tmp;
    if (table[key] != NULL) //jesli znaleznismy element o indeksie key, usuwamy go
    {
        tmp = *table[key];
        table[key] = dummy;
    }else
    {
        cout << "Brak elementu dla podanego klucza! \n";
        return -1;
    }
    
    return tmp;
}

void hash_table_linear::entries()
{

    for (int i = 0; i < SIZE; i++)
    {
        if (table[i] != NULL && table[i] != dummy)
        {
            cout << *table[i] << " ";
        }
    }
    cout << "\n";
}
int * hash_table_linear::find(int number)
{
    int p = 1;
    int key = hash(number);
    bool flag = 0;

    while (*table[hash(key)] != number && p < SIZE) // przeszukujemy tablice dopoki nie znajdziemy number
    {
        key++;
        p++;   
    }
    if (*table[hash(key)] == number)
    {
        cout << "Liczba probek przy przeszukaniu: " << p << endl;
        return table[hash(key)];
    }
    cout << "Nie udalo sie znalezc elementu! " << endl;
    cout << "Liczba probek przy przeszukaniu: " << p << endl;

    return NULL;
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class hash_table_double
{
private:
    int SIZE;
    int prime; //liczba pierwsza miejsza niz rozmiar tablicy
    int **table;
    int * dummy;
    int current_size; //informacja o zapelnieniu tablicy
public:
    hash_table_double(int s)
    {

        table = new int*[s];
        SIZE = s;
        current_size = 0;
        for (int i = 0; i < s; i++)
        {
            table[i] = NULL;
        }

        int tmp = s-1;
        do
        {
            --tmp;
        } while (!check_if_prime(tmp));
        prime = tmp;
    }
    bool isEmpty();
    bool isFull();
    int hash1(int number);
    int hash2(int number);
    void insert(int number);
    int remove(int key);
    int * find(int number);
    void entries();
};

bool hash_table_double::isFull()
{
    if (current_size < SIZE)
    {
        return false;
    }else return true;
}

bool hash_table_double::isEmpty()
{
    bool flag = 0;
    for (int i = 0; i < SIZE; i++)
    {
        if (table[i] != NULL)
        {
            return false;
        } 
    }  
    return true;
}

int hash_table_double::hash1(int number)
{
    return number%SIZE;
}

int hash_table_double::hash2(int number)
{
    return prime - number%prime;
}

void hash_table_double::insert(int number)
{
    if (isFull())
    {
        cout << "Tablica jest pelna, nie mozna dodac elementu!" << endl;
        return;
    }
    
    bool flag = 0;
    int p = 1;
    int key = hash1(number);
    int key2 = hash2(number);
    int key_new;
    int * tmp = new int;
    *tmp = number;
    if (table[key] == NULL || table[key] == dummy) // jesli znajdzie wolne miejsce lub zapelniacz, wpisuje w to miejsce nowa wartosc int
    {
        table[key] = tmp;
    }else // w przeciwnym wypadku uzywana jest funkcja drugiego hashowania do znalezienia wolnego miejsca
    {
        while (!flag)
        {
            key_new = (key + p*key2)%SIZE;
            if (table[key_new] == NULL)
            {
                table[key_new] = tmp;
                flag = 1;
            }
            p++;
        }
    }
    cout << "Liczba probek przy dodawaniu: " << p << endl;
    current_size++;
}

int hash_table_double::remove(int key)
{
    if (key >= SIZE)
    {
        key = hash1(key);
    }
    
    int tmp;
    if (table[key] != NULL)
    {
        tmp = *table[key];
        table[key] = dummy;
    }else
    {
        cout << "Brak elementu dla podanego klucza! \n";
        return -1;
    }
    return tmp;
    current_size--;
}

void hash_table_double::entries()
{

    for (int i = 0; i < SIZE; i++)
    {
        if (table[i] != NULL && table[i] != dummy)
        {
            cout << *table[i] << " ";
        }
    }
    cout << "\n";
}

int * hash_table_double::find(int number)
{
    int p = 1;
    int key = hash1(number);
    bool flag = 0;

    while (*table[hash1(key)] != number && p < SIZE)
    {
        key++;
        p++;   
    }
    if (*table[hash1(key)] == number)
    {
        cout << "Liczba probek przy przeszukaniu: " << p << endl;
        return table[hash1(key)];
    }
    cout << "Liczba probek przy przeszukaniu: " << p << endl;

    return NULL;
}