#include <iostream>

template <typename Type, int Size>
class Vector
{
    Type _tab[Size];

public:
    Vector()
    {
        for (int i = 0; i < Size; i++)
        {
        _tab[i] = 0;
        }
    }
    
    Vector(Vector &vec){for (int i = 0; i < Size; i++) {_tab[i] = vec(i);  } };
    Type operator()(unsigned int i) const { return _tab[i]; }
    Type &operator()(unsigned int i) { return _tab[i]; }
};

template <typename Type, int Size>
class Matrix
{
  Vector<Type, Size> _tab[Size];
public:

  Type operator()(int n, int m) const { return _tab[n](m); }
  Type & operator()(int n, int m)  { return _tab[n](m);  }


};

