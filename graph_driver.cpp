#include <iostream>
#include <string>
#include "graph_m.hh" // zeby zamienic graf na macierzy sasiedztwa na graf na liscie sasiedztwa trzeba zamienic biblioteke i dodac lub usunac _m przy definicji grafu
//#include "graph.hh"
using namespace std;


int main(void)
{
    cout << "Tworze graf o wierzcholkach typu string i krawedziach typu double z macierza sasiedztwa." << endl; 
    graph_m<string, double> g;

    cout << "Dodaje do grafu wierzcholki m1, m2, m3, m4, m5 i wypisuje liste wierzcholkow: " << endl;
    g.insertVertex("m1");g.insertVertex("m2");g.insertVertex("m3");g.insertVertex("m4");g.insertVertex("m5");
    g.verticies();

    cout << "Lacze wierzcholki m1 z m2, m3 z m4 oraz m1 z m5 i wypisuje liste krawedzi:" << endl;
    g.insertEdge("m1","m2",25);g.insertEdge("m3","m4",50);g.insertEdge("m1","m5",10);
    g.edges();

    cout << "Krawedzie przylegajace do m1: " << endl;
    g.incidentEdges(g.find_vertex("m1"));

    cout << "Wypisuje tablice koncowych wierzcholkow krawedzi 50: " << endl;
    cout << g.end_verticies(g.find_edge(50))[0].val << " " << g.end_verticies(g.find_edge(50))[1].val << endl;

    cout << "Wypisuje przeciwlegly wierzcholek do m3 wzgledem 50: " << endl;
    cout << g.opposite(g.find_vertex("m3"),g.find_edge(50))->val << endl; 

    cout << "Czy m3 i m4 sa sasiednie: " << g.are_Adjacent(g.find_vertex("m3"),g.find_vertex("m4")) << endl;
    cout << "Czy m3 i m1 sa sasiednie: " << g.are_Adjacent(g.find_vertex("m3"),g.find_vertex("m1")) << endl;

    cout << "Zamieniam element w wierzcholku m1 na m0: " << endl;
    g.verticies();
    g.replace_v(g.find_vertex("m1"),"m0");
    g.verticies();

    cout << "Zamieniam element w krawedzi 50 na 49: " << endl;
    g.replace_e(g.find_edge(50),49);
    g.edges();

    cout << "Usuwam krawedz 25: " << endl;
    g.edges();
    g.removeEdge(25);
    g.edges();

    cout << "Usuwam wierzcholek m0: " << endl;
    g.verticies();
    g.edges();
    g.removeVertex("m0");
    g.verticies();
    g.edges();
}