#include <iostream>
#include <time.h>
#include "hash_tables.hh"

using namespace std;

int main(void)
{
    srand( time( NULL ) );

    int s;
    cout << "Podaj wielkosc tablicy (liczbe pierwsza): ";
    cin >> s;
    int tab[s+1];
    
    int tmp = s;
    if (!check_if_prime(s))
    {
        cout << "Podana liczba nie jest liczba pierwsza, szukam kolejnej liczby pierwszej." << endl;
        do
        {
            ++tmp;
        } while (!check_if_prime(tmp));
        s = tmp;
        cout << "Znalazlem liczbe: " << s << endl;
    }

    cout << "Generuje " << s+1 << " losowych liczb bez powtorzen: " << endl;

    for (int i = 0; i < s+1; i++)
    {
        tmp = rand()%200;
        for (int j = 0; j < i; j++)
        {
            if (tab[s] == tmp)
            {
                i--;
            }
        }
        tab[i] = tmp;
    }
    for (int i = 0; i < s+1; i++)
    {
        cout << tab[i] << " ";
    }
    cout << endl;

    cout << "i probuje je wpisac do tablicy hashujacej: " << endl;

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////

    hash_table h(s);
    
    for (int i = 0; i < s+1; i++)
    {
        h.insert(tab[i]);
    }

    cout << endl << "---------------------------" << endl
    << "Tablica z linkowaniem" << endl;
    cout << "Wypisuje zawartosc tablicy: " << endl;
    h.entries();
    cout << "Usuwam element " << tab[s/5] << " i wypisuje zawartosc tablicy: ";
    h.remove(tab[s/5]);
    cout << endl;
    h.entries();
    cout << "Szukam elementu " << tab[s/5] << ": ";
    if (h.find(tab[s/5]) != NULL)
    {
        cout << h.find(tab[s/5])->val  << endl;
    }
    cout << endl << "---------------------------" << endl;
    tmp = rand()%s; // pomocnicza wartosc, wybiera losowy element tablicy do znalezienia

    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    hash_table_linear g(s);
    
    for (int i = 0; i < s+1; i++)
    {
        g.insert(tab[i]);
    }
    cout << endl << "---------------------------" << endl
    << "Tablica z probkowaniem liniowym" << endl;
    cout << "Wypisuje zawartosc tablicy: " << endl;
    g.entries();
    cout << "Usuwam element o kluczu " << tmp << " i wypisuje zawartosc tablicy: " << endl;
    g.remove(tmp);
    g.entries();
    cout << "Szukam elementu " << tab[s/5] << ": " << endl;
    if (g.find(tab[s/5]) != NULL)
    {
        cout << *g.find(tab[s/5]) << endl;
    }
    cout << endl << "---------------------------" << endl;
    /////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////
    hash_table_double f(s);
    
    for (int i = 0; i < s+1; i++)
    {
        f.insert(tab[i]);
    }
    cout << endl << "---------------------------" << endl
    << "Tablica z dwoma funkcjami haszujacymi" << endl;
    cout << "Wypisuje zawartosc tablicy: " << endl;
    f.entries();
    cout << "Usuwam element o kluczu " << tmp << " i wypisuje zawartosc tablicy: " << endl;
    f.remove(tmp);
    f.entries();
    cout << "Szukam elementu " << tab[s/5] << ": " << endl;
    if (f.find(tab[s/5]) != NULL)
    {
        cout << *f.find(tab[s/5]) << endl;
    }
    cout << "---------------------------" << endl;
}