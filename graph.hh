#include <iostream>
#include <string>
#include "list.hh"

template <typename Type, typename eType> 
class edge; 

template <typename Type, typename eType> //wierzcholek
struct vertex
{
    Type val; // nazwa wierzcholka
    element<vertex<Type,eType>*>* v_list_elem; // wskaznik na liste zawierajaca wszystkie wierzcholki
    list<edge<Type,eType>*> i_list; // wskaznik na liste incydencji danego wierzcholka
};

template <typename Type, typename eType>  //krawedz
class edge
{
    public:
    eType val; // dlugosc krawedzi
    vertex<Type,eType>* e1; // wskaznik na pierwszy wierzcholek
    vertex<Type,eType>* e2; // wskaznik na drugi wierzcholek
    element<edge<Type,eType>*>* e_list_elem; // wskaznik na liste zawierajaca wszyskie krawedzie
    element<edge<Type,eType>*>* i1; // wskaznik na liste incydencji pierwszego wierzcholka
    element<edge<Type,eType>*>* i2; // wskaznik na liste incydencji drugiego wierzcholka
};


template <typename Type, typename eType> // graf
class graph
{
private: 
    list<edge<Type,eType>*> e_list; // lista krawedzi
    list<vertex<Type,eType>*> v_list; //lista wierzcholkow
public:
    
    bool isEmpty(); // sprawdza czy graf jest pusty
    vertex<Type,eType> * find_vertex(Type val); // znajduje wskaźnik na wierzcholek o nazwie 'val'
    edge<Type,eType> * find_edge(eType val); // znajduje wskaźnik na krawedz o dlugosci 'val'
    void verticies(); // wypisuje wszystkie wierzcholki
    void edges(); // wypisuje wszystkie krawedzie
    void incidentEdges(vertex<Type,eType>* v); // wypisuje krawedzie przypisane do wierzcholka v
    void insertVertex(Type o); // dodaje wierzcholek o nazwie o
    void insertEdge(Type v, Type w, eType o); // dodaje krawedz e pomiedzy wierzcholkami v i w
    vertex<Type,eType>* end_verticies(edge<Type,eType> * e); // zwraca tablice wierzcholkow na koncach krawedzi e
    vertex<Type,eType>* opposite(vertex<Type,eType>* v, edge<Type,eType>* e); // zwraca wierzcholek przeciwny do v wzgledem krawedzi e
    bool are_Adjacent(vertex<Type,eType>* v, vertex<Type,eType>* w); // sprawdza czy v i w maja wspolna krawedz
    void replace_v(vertex<Type,eType>* v, Type val); // zamienia nazwe wierzcholka v na wartosc val
    void replace_e(edge<Type,eType>* e, eType val); // zamienia dlugosc krawedzi e na wartosc val
    void removeVertex(Type o);
    void removeEdge(eType o);
};

template <typename Type, typename eType> 
bool graph<Type,eType>::isEmpty()
{
    if (v_list.isEmpty()) // sprawdza liste wierzcholkow
    {
        return 1;
    }else return 0;
}

template <typename Type, typename eType> 
vertex<Type,eType>* graph<Type,eType>::find_vertex(Type val)
{
    vertex<Type,eType> n;
    n.val = val;
    vertex<Type,eType> *m;
    m = &n; 
    return v_list.find_ptr_elem(m)->val; // przeszkuje liste wierzcholkow w poszukiwaniu tego o nazwie val
}

template <typename Type, typename eType> 
edge<Type,eType>* graph<Type,eType>::find_edge(eType val)
{
    edge<Type,eType> n;
    n.val = val;
    edge<Type,eType>* m = &n;
    return e_list.find_ptr_elem(m)->val; // przeszukuje liste krawedzi w poszukiwaniu tego o dlugosci val
}

template <typename Type, typename eType> 
void graph<Type,eType>::verticies()
{
    if(!v_list.isEmpty())
    {
        element<vertex<Type,eType>*>* n = v_list.get_head();
        
        while (n != NULL) // jesli lista wierzcholkow nie jest pusta wypisuje nazwy wszystkich wierzcholkow ktore sa jej elementami
        {
            std::cout << (n->val)->val << " ";
            n = n->next;
        }
        std::cout << "\n";   
    }else 
    {
        std::cout << "Brak wierzcholkow \n";
    }
}

template <typename Type, typename eType> 
void graph<Type,eType>::edges()
{
    if(!e_list.isEmpty())
    {
        
        element<edge<Type,eType>*>* n = e_list.get_head();

        while (n != NULL) // jesli lista krawedzi nie jest pusta wypisuje dlugosci wszystkich krawedzi ktore sa jej elementami
        {
            std::cout << (n->val->val) << " ";
            n = n->next;
        }
        std::cout << "\n";
    }else
    {
        std::cout << "Brak krawedzi \n";
    }
}

template <typename Type, typename eType> 
void graph<Type,eType>::incidentEdges(vertex<Type,eType>* v)
{
    if (!v_list.isEmpty()) //sprawdza czy graf ma wierzcholki
    {
      if (!v->i_list.isEmpty()) // sprawdza czy wierzcholek v ma jakies krawedzie
      {
        element<edge<Type,eType>*>* n = v->i_list.get_head();
        while (n != NULL) // wypisuje dlugosci krawedzi polaczone z wierzcholkiem v
        {
            std::cout << n->val->val << ", ";
            n = n->next;
        }
        std::cout << "\n";
      }
    }
    
}

template <typename Type, typename eType> 
void graph<Type,eType>::insertVertex(Type o)
{
    vertex<Type,eType>* v = new vertex<Type,eType>;
    v->val = o;
    v_list.add_front(v); // dodaje nowy wierzcholek do list wierzcholkow
    v->v_list_elem = v_list.find_elem(v);
}

template <typename Type, typename eType> 
void graph<Type,eType>::insertEdge(Type v, Type w, eType o)
{
    edge<Type,eType> * e = new edge<Type,eType>;
    vertex<Type,eType> * ver = new vertex<Type,eType>;
    ver->val = v;
    e->val = o;

    e_list.add_front(e); // dodaje nowa krawedz do listy krawedzi

    e->e_list_elem = e_list.find_elem(e); // ustawianie wskaznikow krawedzi na wierzcholki, z oraz na liste incydencji
    e->e1 = v_list.find_ptr_elem(ver)->val;
    e->e1->i_list.add_front(e);
    e->i1 = e->e1->i_list.find_ptr_elem(e);

    ver->val = w;
    e->e2 = v_list.find_ptr_elem(ver)->val;
    e->e2->i_list.add_front(e);
    e->i2 = e->e2->i_list.find_ptr_elem(e);

    delete ver;
}

template <typename Type, typename eType> 
vertex<Type,eType>* graph<Type,eType>::end_verticies(edge<Type,eType> *e)
{
    vertex<Type,eType>* tab = new vertex<Type,eType>[2]; //tablica wierzcholkow przy krawedzi e
    tab[1] = *e->e1;
    tab[0] = *e->e2;

    return tab;    
}

template <typename Type, typename eType> 
vertex<Type,eType>* graph<Type,eType>::opposite(vertex<Type,eType>* v, edge<Type,eType>* e)
{
    if (e->e1->val == v->val)
    {
        return e->e2;
    }else return e->e1; //wierzcholek przeciwny do v wzgledem krawedzi e

}

template <typename Type, typename eType>
bool graph<Type,eType>::are_Adjacent(vertex<Type,eType>* v, vertex<Type,eType>* w)
{
    bool flag = 0;

    element<edge<Type,eType>*> * n; 
    n = v->i_list.get_head();

    while (flag != 1 && n != NULL)
    {
        if ( n->val->e1->val == w->val || n->val->e2->val == w->val )
        {
            flag = 1; // prawdziwe jesli wierzcholki sa polaczone jedna krawedzia
        }
        n = n->next;
    }

    return flag;
}

template <typename Type, typename eType>
void graph<Type,eType>::replace_v(vertex<Type,eType>* v, Type val)
{
    v->val = val;
}

template <typename Type, typename eType>
void graph<Type,eType>::replace_e(edge<Type,eType>* e, eType val)
{
    e->val = val;
}

template <typename Type, typename eType>
void graph<Type,eType>::removeVertex(Type o)
{
    vertex<Type,eType> * n = find_vertex(o);
    element<edge<Type,eType>*> *m = n->i_list.get_head();
    
    while (m != NULL)
    {
        removeEdge(n->i_list.get_head()->val->val);
        m = m->next;
    }
    v_list.remove(n->v_list_elem->val);

    delete n;

}

template <typename Type, typename eType>
void graph<Type,eType>::removeEdge(eType o)
{
    edge<Type,eType>* n = find_edge(o);

    e_list.remove(n->e_list_elem->val);
    n->e1->i_list.remove(n->i1->val);
    n->e2->i_list.remove(n->i2->val);

    delete n;
}
