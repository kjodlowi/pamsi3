#include <iostream>
#include <string>

template <typename Type> 
struct element
{
    Type val;
    element<Type>* next;
    element<Type>* prev;
};

template <typename Type>
class list
{
private:
    element<Type>* head;
public:

    list(){head = NULL;};
    ~list(){
        element<Type>* tmp;
        while (head)
        {
            tmp = head;
            head = head->next;
            delete tmp;
        }
    };


    element<Type>* get_head(){return head;};
    bool isEmpty();
    void print_list();
    element<Type>* find_elem(Type val);
    element<Type>* find_ptr_elem(Type val);
    void add_front(Type new_val);
    void add_end(Type new_val);
    void add_after(element<Type>* prev_elem,Type new_val);
    void add_before(element<Type>* next_elem,Type new_val);
    void remove(Type val);
};

template <typename Type>
bool list<Type>::isEmpty()
{
    if (head == NULL) return 1;
    else return 0;
}

template <typename Type>
void list<Type>::print_list()
{
    if(!isEmpty())
    {
        element<Type>* n;
        n = head;
        while (n != NULL)
        {
            std::cout << n->val<< " ";
            n = n->next;
        }
        std::cout << "\n"; 
    }
}

template <typename Type>
element<Type>* list<Type>::find_elem(Type val)
{
    element<Type>* n = head;

    bool flag = 0;

    while (n != NULL && flag != 1)
    {
        if (n->val == val)
        {
            flag = 1;
        }else n = n->next;
    }

    if (flag < 1) 
    {
        std::cout << "Nie znaleziono elementu" << std::endl;
        return NULL; 
    }else
    {
        return n;
    } 
}

template <typename Type>
element<Type>* list<Type>::find_ptr_elem(Type val)
{
    element<Type>* n = head;

    bool flag = 0;

    while (n != NULL && flag != 1)
    {
        if (n->val->val == val->val)
        {
            flag = 1;
        }else n = n->next;
    }

    if (flag < 1) 
    {
        std::cout << "Nie znaleziono elementu" << std::endl;
        return NULL; 
    }else
    {
        return n;
    } 
}

template <typename Type>
void list<Type>::add_front(Type new_val)
{
    element<Type> * elem = new element<Type>;

    elem->val = new_val;
    elem->next = head;
    elem->prev = NULL;

    if (head != NULL)
    {
        head->prev = elem;
    }

    head = elem;
}

template <typename Type>
void list<Type>::add_end(Type new_val)
{
    element<Type> * elem = new element<Type>;

    elem = head;
    while (elem->next != NULL)
    {
        elem = elem->next;
    }
    add_after(elem,new_val);
}

template <typename Type>
void list<Type>::add_after(element<Type>* prev_elem, Type new_val)
{
    if(prev_elem == NULL)
    {
        std::cout << "Nie mozna dodac elementu" << std::endl;
        return;
    }

    element<Type>* elem = new element<Type>;
    
    elem->val = new_val;

    elem->next = prev_elem->next;

    prev_elem->next = elem;

    elem->prev = prev_elem;
    
    if(elem->next != NULL) elem->next->prev = elem;
}

template <typename Type>
void list<Type>::add_before(element<Type>* next_elem, Type new_val)
{
    if(next_elem == NULL)
    {
        std::cout << "Nie mozna dodac elementu" << std::endl;
        return;
    }
    
    element<Type>* elem = new element<Type>;

    elem->val = new_val;
    elem->prev = next_elem->prev;
    next_elem->prev = elem;
    elem->next = next_elem;

    if (elem->prev != NULL)
    {
        elem->prev->next = elem;
    }else
    {
        head = elem;
    }
    
}

template <typename Type>
void list<Type>::remove(Type val)
{
    element<Type> * tmp = find_elem(val);
    if (get_head() == tmp)
    {
        if (tmp->next != NULL)
        {
            head = tmp->next;
        }else
        {
            head = NULL;
        } 
    }
    
    if (tmp->next != NULL && tmp->prev != NULL)
    {
        tmp->next->prev = tmp->prev;
        tmp->prev->next = tmp->next;
    }else
    {
        if (tmp->next != NULL)
        {
            tmp->next->prev = NULL;
        }
        if (tmp->prev != NULL)
        {
            tmp->prev->next = NULL;
        }
    }
    delete tmp;
}